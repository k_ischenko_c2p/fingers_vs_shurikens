﻿using UnityEngine;


public class WeaponThrower : MonoBehaviour
{
    [SerializeField] private Transform _weapon;
    
    private float _currentIntesity;
    private float _currentThrowTime;
    private float _throwHoldTime;
    
    private float _minWeaponCountPerSecond = 1;
    private float _maxWeaponCountPerSecond = 10;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_currentIntesity <= 0f)
        {
            _currentThrowTime = float.PositiveInfinity;
            return;
        }

        _currentThrowTime += Time.deltaTime;
            
        if (_currentThrowTime <= _throwHoldTime)
        {
            return;
        }

        _currentThrowTime = 0f;

        var go = Instantiate(_weapon, RandomPointOnCircleEdge(5), Quaternion.identity);
        Destroy(go.gameObject, 1f);
    }

    private Vector3 RandomPointOnCircleEdge(float radius)
    {
        var vector2 = Random.insideUnitCircle.normalized * radius;
        return new Vector3(vector2.x, vector2.y, 0);
    }
    
    public void SetIntensity(float level)
    {
        var count = Mathf.Lerp(_minWeaponCountPerSecond, _maxWeaponCountPerSecond, level);
        _throwHoldTime = 1.0f / count;
        _currentIntesity = level;
    }
}
