﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ObstaclesGenerator : MonoBehaviour
{
    [SerializeField] private Transform _obstaclePrefab;
    [SerializeField] private Player _player;

    private Dictionary<ObstacleOrigin, Rect> _originRects;
    
    private float _currentIntesity;
    private float _currentThrowTime;
    private float _generationFrequency;
    
    private float _minWeaponCountPerSecond = 1;
    private float _maxWeaponCountPerSecond = 10;
    
    void Awake()
    {
        _originRects = new Dictionary<ObstacleOrigin, Rect>();
        _originRects.Add(ObstacleOrigin.Left, new Rect(-12f, -8f, -9f, 8f));
        _originRects.Add(ObstacleOrigin.Top, new Rect(-9f, -8f, 9f, -5f));
        _originRects.Add(ObstacleOrigin.Right, new Rect(9f, -8f, 12f, 8f));
        _originRects.Add(ObstacleOrigin.Bottom, new Rect(-9f, 5f, 9f, 8f));

    }

    void Update()
    {
        if (_currentIntesity <= 0f)
        {
            _currentThrowTime = float.PositiveInfinity;
            return;
        }

        _currentThrowTime += Time.deltaTime;
            
        if (_currentThrowTime <= _generationFrequency)
        {
            return;
        }
        
        var obstacleOrigin = SelectObstacleOrigin();
        var obstaclePosition = GenerateObstacleOriginalPosition(obstacleOrigin);

        var obstacleTransform = Instantiate(_obstaclePrefab, obstaclePosition, Quaternion.identity);

        Destroy(obstacleTransform.gameObject, 5f);
        var movement = obstacleTransform.gameObject.GetComponent<ObstacleMovement>();
        movement.ObstacleTarget = _player.transform.position;

        _currentThrowTime = 0f;
    }

    private ObstacleOrigin SelectObstacleOrigin()
    {
        var position = Random.Range(1, 5);
        return (ObstacleOrigin)position;
    }

    private Vector3 GenerateObstacleOriginalPosition(ObstacleOrigin origin)
    {
        var originalRect = _originRects[origin];

        var obstacleStartX = Random.Range(originalRect.x, originalRect.width);
        var obstacleStartY = Random.Range(originalRect.y, originalRect.height);

        return new Vector3(obstacleStartX, obstacleStartY, 0);
    }
    
    public void SetIntensity(float level)
    {
        var count = Mathf.Lerp(_minWeaponCountPerSecond, _maxWeaponCountPerSecond, level);
        _generationFrequency = 1.0f / count;
        _currentIntesity = level;
    }
}

public enum ObstacleOrigin {
    None,
    Left,
    Top,
    Right,
    Bottom
}
