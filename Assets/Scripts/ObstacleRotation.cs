﻿using UnityEngine;

public class ObstacleRotation : MonoBehaviour
{
    private int _rotationZ;
    private int _rotationSpeed = 8;

    void Update()
    {
        _rotationZ += _rotationSpeed;

         this.transform.rotation = Quaternion.Euler(0, 0, _rotationZ);    
    }
}
