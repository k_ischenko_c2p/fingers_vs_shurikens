﻿using UnityEngine;

public class ObstacleMovement : MonoBehaviour
{
    public Vector2 ObstacleTarget;

    private float _minAbsoluteSpeed = 0.05f;
    private float _maxAbsoluteSpeed = 0.15f;
    private float _speedFactor;

    void Start()
    {
        var distanceX = ObstacleTarget.x - this.transform.position.x;
        var distanceY = ObstacleTarget.y - this.transform.position.y;

        _speedFactor = distanceX / distanceY;
    }

    void Update()
    {
        float speedX;
        float speedY;

        var currentSpeed = Random.Range(_minAbsoluteSpeed, _maxAbsoluteSpeed);

        if (Mathf.Abs(_speedFactor) >= 1)
        {
            speedX = currentSpeed;
            speedY = currentSpeed / _speedFactor;
        }
        else
        {
            speedX = currentSpeed * _speedFactor;
            speedY = currentSpeed;
        }

        var originalPosition = this.transform.position;
            var nextPosition = new Vector3(originalPosition.x+speedX, originalPosition.y+speedY, 0);
            
            this.transform.position = nextPosition;            
    }
}
