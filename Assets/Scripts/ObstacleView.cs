﻿using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(AudioSource))]
public class ObstacleView : MonoBehaviour
{
    [SerializeField] private Sprite[] obstacleSprites;

    private AudioSource _audioSource;
    
    void Awake()
    {
        SelectObstacleSprite();
        SetObstacleScale();

        _audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        _audioSource.Play();
    }

    private void SelectObstacleSprite()
    {
       var renderer = this.GetComponent<SpriteRenderer>();
       var spriteId = Random.Range(0, obstacleSprites.Length);

        renderer.sprite = obstacleSprites[spriteId];
    }

    private void SetObstacleScale()
    {
        this.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
    }
}
