﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Init,
    Start,
    Playing,
    End
}

public class GameLogic : MonoBehaviour
{
    [SerializeField] private Player _player;
    [SerializeField] private ObstaclesGenerator _weaponThrower;
    
    public GameState GameState;
    public float Score;

    private float _maxLevel = 60f;
    
    // Update is called once per frame
    void Update()
    {
        if (GameState == GameState.End)
        {
            _player.IsReadyToPlay = false;
            _weaponThrower.SetIntensity(0);
            
            return;
        }

        if (GameState == GameState.Init)
        {
            _player.IsReadyToPlay = true;
            _player.Reset();
            _weaponThrower.SetIntensity(0);
            
            GameState = GameState.Start;
        }

        if (GameState == GameState.Start)
        {
            if (!_player.IsAlive)
            {
                return;
            }

            Score = 0;
            GameState = GameState.Playing;
        }

        if (GameState == GameState.Playing)
        {
            Score += Time.deltaTime;
            _weaponThrower.SetIntensity(Mathf.Clamp01(Score / _maxLevel));
            
            if (_player.IsAlive)
            {
                return;
            }
            GameState = GameState.End;
        }
    }
}
