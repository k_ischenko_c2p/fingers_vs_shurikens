﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameLogic))]
public class GameUiController : MonoBehaviour
{
    private GameLogic _gameLogic;
    
    // Start is called before the first frame update
    void Start()
    {
        _gameLogic = GetComponent<GameLogic>();
    }

    public void PlayAgainClicked()
    {
        _gameLogic.GameState = GameState.Init;
    }
}
