﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof(GameLogic))]
[RequireComponent(typeof(AudioSource))]
public class GameUiView : MonoBehaviour
{
    [SerializeField] private GameObject _scorePopup;
    [SerializeField] private TextMeshProUGUI _scoreValue;
    
    private GameLogic _gameLogic;
    private GameState _gameState;
    private AudioSource _audioSource;
    
    void Start()
    {
        _gameLogic = GetComponent<GameLogic>();
        _gameState = GameState.Init;
        _audioSource = GetComponent<AudioSource>();
    }
    
    // Update is called once per frame
    void Update()
    {
        if (_gameState == _gameLogic.GameState)
        {
            return;
        }

        _gameState = _gameLogic.GameState;
        _scorePopup.SetActive(_gameState == GameState.End);
        _scoreValue.text = string.Format("{0} sec", _gameLogic.Score);
    }
}
