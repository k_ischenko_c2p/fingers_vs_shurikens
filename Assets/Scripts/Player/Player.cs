﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public bool IsTouch;
    public bool WasHit;
    public bool IsReadyToPlay;
    
    public void Reset()
    {
        IsTouch = false;
        WasHit = false;
    }

    public bool IsAlive
    {
        get { return IsTouch && !WasHit; }
    } 
}
