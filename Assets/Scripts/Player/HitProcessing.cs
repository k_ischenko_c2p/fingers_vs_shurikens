﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Player))]
public class HitProcessing : MonoBehaviour
{
    private Player _player;
    
    // Start is called before the first frame update
    void Start()
    {
        _player = GetComponent<Player>();
    }

    void OnTriggerEnter2D(Collider2D collider2D)
    {
        _player.WasHit = true;
    }
}
