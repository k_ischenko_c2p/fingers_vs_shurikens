﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Player))]
public class PlayerView : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;
    private Player _player;
    private bool _playerIsTouchState;
    private bool _playersIsReadyToPlay;
    
    // Start is called before the first frame update
    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _player = GetComponent<Player>();
        
        _playerIsTouchState = !_player.IsTouch;
        _playersIsReadyToPlay = !_playersIsReadyToPlay;
    }

    // Update is called once per frame
    void Update()
    {
        if (_playerIsTouchState == _player.IsTouch &&
            _playersIsReadyToPlay == _player.IsReadyToPlay)
        {
            return;
        }
        
        _playerIsTouchState = _player.IsTouch;
        _playersIsReadyToPlay = _player.IsReadyToPlay;
            
        var color = _spriteRenderer.color;
        color.a = _playerIsTouchState && _player.IsReadyToPlay ? 1 : 0;
        _spriteRenderer.color = color;
    }
}
