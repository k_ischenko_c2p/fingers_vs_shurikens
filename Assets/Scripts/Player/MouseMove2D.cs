﻿using UnityEngine;
 
[RequireComponent(typeof(Player))]
public class MouseMove2D : MonoBehaviour
{
    private Camera _camera;
    private Player _player;
    
    private Vector3 mousePosition;
 
    // Use this for initialization
    void Start ()
    {
        _camera = Camera.main;
        _player = GetComponent<Player>();
    }
   
    // Update is called once per frame
    void Update ()
    {
        _player.IsTouch = Input.GetMouseButton(0);
        
        if (_player.IsTouch) 
        {
            var pos = _camera.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(pos.x, pos.y, 0);
        }
    }
}